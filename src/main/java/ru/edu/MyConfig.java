package ru.edu;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import ru.edu.deposit.DepositDao;

@Configuration
@EnableJpaRepositories("ru.edu.repository")
public class MyConfig {

    @Bean
    public DepositDao myDao() {
        return new DepositDao();
    }
}
