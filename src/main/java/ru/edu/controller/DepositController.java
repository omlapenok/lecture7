package ru.edu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.edu.deposit.DepositDao;
import ru.edu.deposit.DepositInfo;


@Controller
public class DepositController {

    public DepositDao depositDao;

    @Autowired
    public void setDepositDao(DepositDao depositDao) {
        this.depositDao = depositDao;
    }

    @PostMapping(value = "/newDeposit")
    public ResponseEntity<?> create(@RequestBody DepositInfo depositInfo) {
        depositDao.createDeposit(depositInfo);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(value = "/info/{id}")
    public String info(@PathVariable(name = "id") Integer id, Model model){
        model.addAttribute("name",depositDao.depositInfo(id));
        return "hello";
    }

    @DeleteMapping(value = "/delete/{id}")
    public String delete(@PathVariable(name = "id") Integer id, Model model) {
        String response = "";
        if(depositDao.deleteDeposit(id)){
            response = "Success";
        } else response = "Error!";

        model.addAttribute("name", response);
        return "hello";
    }
}
