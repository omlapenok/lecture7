package ru.edu.deposit;

import java.util.Optional;

public interface DepositDaoImpl {

    /**
     * Создание вклада
     * @param depositInfo
     */
    void createDeposit(DepositInfo depositInfo);

    /**
     * Просмотр накоплений по вкладу
     * @param id
     * @return DepositInfo
     */
    DepositInfo depositInfo(Integer id);

    /**
     * Удаление вклада
     * @param id
     * @return false - если вклада с таким id не существует
     */
    boolean deleteDeposit(Integer id);
}
