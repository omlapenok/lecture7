package ru.edu.deposit;

import javax.persistence.*;

@Entity
@Table(name = "depositsInfo")
public class DepositInfo {
    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "nameOwner")
    private String nameOwner;

    @Column(name = "amount")
    private Integer amount;

    @Column(name = "currency")
    private String currency;

    public DepositInfo() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameOwner() {
        return nameOwner;
    }

    public void setNameOwner(String nameOwner) {
        this.nameOwner = nameOwner;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        return "DepositInfo{" +
                "id=" + id +
                ", nameOwner='" + nameOwner + '\'' +
                ", amount=" + amount +
                ", currency='" + currency + '\'' +
                '}';
    }
}
