package ru.edu.deposit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.edu.repository.DepositsRepository;

import java.util.Optional;

@Service
@PropertySource("db.properties")
public class DepositDao implements DepositDaoImpl{

    @Autowired
    private DepositsRepository depositsRepository;


    /**
     * Создание вклада
     * @param depositInfo
     */
    @Override
    public void createDeposit(DepositInfo depositInfo) {

        depositsRepository.save(depositInfo);
    }

    /**
     * Просмотр накоплений по вкладу
     * @param id
     * @return DepositInfo
     */
    @Override
    public DepositInfo depositInfo(Integer id) {
        if(depositsRepository.existsById(id)) {
            return depositsRepository.getById(id);
        }else throw new RuntimeException("Deposit with id not found");

    }

    /**
     * Удаление вклада
     * @param id
     */
    @Override
    public boolean deleteDeposit(Integer id) {
        if(depositsRepository.existsById(id)) {
            depositsRepository.deleteById(id);
            return true;
         } else return false;
    }
}
