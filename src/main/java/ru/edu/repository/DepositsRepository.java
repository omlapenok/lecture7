package ru.edu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.edu.deposit.DepositInfo;

@Repository
public interface DepositsRepository extends JpaRepository<DepositInfo, Integer> {
}
